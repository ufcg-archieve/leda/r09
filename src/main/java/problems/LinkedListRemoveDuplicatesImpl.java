package problems;

import adt.linkedList.SingleLinkedListNode;

public class LinkedListRemoveDuplicatesImpl<T> implements LinkedListRemoveDuplicates<T>{

    /**
     * Restricoes extras:
     * - Você NÃO pode usar recursão
     * - Você pode criar métodos auxiliares se achar necessário, desde que sejam criados
     *   nesta classe
     */
    public void removeDuplicates(SingleLinkedListNode<T> node){
        if (node != null) {
            SingleLinkedListNode<T> aux = node;
            while (!aux.isNIL()) {
                SingleLinkedListNode<T> anterior = aux;
                SingleLinkedListNode<T> proximo = aux.getNext();
                while (!proximo.isNIL()) {
                    if (proximo.equals(aux)) {
                        removeProximo(anterior);
                    } else {
                        anterior = anterior.getNext();
                    }
                    proximo = proximo.getNext();
                }
                aux = aux.getNext();
            }
        }
    }

    private void removeProximo(SingleLinkedListNode<T> node) {
        node.setNext(node.getNext().getNext());
    }
}
